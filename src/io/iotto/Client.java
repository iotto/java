package io.iotto;
//package jota;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import io.iotto.Block;
import io.iotto.PowerBall;

/**
 * Created by bioduds on 06/08/17.
 */
public class Client {

  private PowerBall game = new PowerBall();

  private JButton btnMain;
  private JPanel panelMain;
  private JLabel mainLabel;
  private JButton button1;

  private Color DEFAULT = new Color(238, 238, 238);
  private Color GREEN = new Color(51, 204, 51);

  private JButton
      a1Btn, a2Btn, a3Btn, a4Btn, a5Btn, a6Btn, a7Btn, a8Btn, a9Btn, a10Btn,
      a11Btn, a12Btn, a13Btn, a14Btn, a15Btn, a16Btn, a17Btn, a18Btn, a19Btn, a20Btn,
      a21Btn, a22Btn, a23Btn, a24Btn, a25Btn, a26Btn, a27Btn, a28Btn, a29Btn, a30Btn,
      a31Btn, a32Btn, a33Btn, a34Btn, a35Btn, a36Btn, a37Btn, a38Btn, a39Btn, a40Btn,
      a41Btn, a42Btn, a43Btn, a44Btn, a45Btn, a46Btn, a47Btn, a48Btn, a49Btn, a50Btn,
      a51Btn, a52Btn, a53Btn, a54Btn, a55Btn, a56Btn, a57Btn, a58Btn, a59Btn, a60Btn,
      a61Btn, a62Btn, a63Btn, a64Btn, a65Btn, a66Btn, a67Btn, a68Btn, a69Btn,
      p1Btn, p2Btn, p3Btn, p4Btn, p5Btn, p6Btn, p7Btn, p8Btn, p9Btn, p10Btn,
      p11Btn, p12Btn, p13Btn, p14Btn, p15Btn, p16Btn, p17Btn, p18Btn, p19Btn, p20Btn,
      p21Btn, p22Btn, p23Btn, p24Btn, p25Btn, p26Btn;
  private JLabel chosenGameLabel;
  private JLabel pbLabel;

  private ArrayList<JButton> aBtns = new ArrayList< JButton >();
  private ArrayList<JButton> pBtns = new ArrayList< JButton >();

  public Client() {

    // populate aBtns ArrayList
    aBtns.addAll( Arrays.asList(
        a1Btn, a2Btn, a3Btn, a4Btn, a5Btn, a6Btn, a7Btn, a8Btn, a9Btn, a10Btn,
        a11Btn, a12Btn, a13Btn, a14Btn, a15Btn, a16Btn, a17Btn, a18Btn, a19Btn, a20Btn,
        a21Btn, a22Btn, a23Btn, a24Btn, a25Btn, a26Btn, a27Btn, a28Btn, a29Btn, a30Btn,
        a31Btn, a32Btn, a33Btn, a34Btn, a35Btn, a36Btn, a37Btn, a38Btn, a39Btn, a40Btn,
        a41Btn, a42Btn, a43Btn, a44Btn, a45Btn, a46Btn, a47Btn, a48Btn, a49Btn, a50Btn,
        a51Btn, a52Btn, a53Btn, a54Btn, a55Btn, a56Btn, a57Btn, a58Btn, a59Btn, a60Btn,
        a61Btn, a62Btn, a63Btn, a64Btn, a65Btn, a66Btn, a67Btn, a68Btn, a69Btn
    ));
    // populate pBtns ArrayList
    pBtns.addAll( Arrays.asList(
        p1Btn, p2Btn, p3Btn, p4Btn, p5Btn, p6Btn, p7Btn, p8Btn, p9Btn, p10Btn,
        p11Btn, p12Btn, p13Btn, p14Btn, p15Btn, p16Btn, p17Btn, p18Btn, p19Btn, p20Btn,
        p21Btn, p22Btn, p23Btn, p24Btn, p25Btn, p26Btn
    ));
    // addListeners
    for ( JButton aBtn : aBtns ) {
      aBtn.setBackground( DEFAULT );
      aBtn.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed( ActionEvent e ) { processListener(e); }
      });
    }
    for ( JButton pBtn : pBtns ) {
      pBtn.setBackground( DEFAULT );
      pBtn.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed( ActionEvent e ) { processListener(e); }
      });
    }

    btnMain.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        // code
        //JOptionPane.showMessageDialog(null,"Hello World" );

        File dir = new File("blocks");
        dir.mkdir();

        PrintWriter pw = null;

        try {
          File file = new File("blocks/tb.dat");
          FileWriter fw = new FileWriter(file, true);
          pw = new PrintWriter(fw);
          pw.println( "iotto rules!!" );
        } catch (IOException el) {
          el.printStackTrace();
        } finally {
          if (pw != null) {
            pw.close();
          }
        }
      }
    });

    button1.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        Timestamp ts = new Timestamp( System.currentTimeMillis() );
        Block block = new Block( ts );
        block.checkBlock();
      }
    });

  }

  /**
   *
   * @param e
   */
  private void processListener( ActionEvent e ) {
    JButton btn = (JButton) e.getSource();
    String toolTip[] = new String[3];
    toolTip = btn.getToolTipText().split( " " );

    // Ball Chosen
    if( toolTip[0].equals( "Ball" ) ) {
      Integer cb = Integer.parseInt( toolTip[1] );
      String resultOfChosingBall = this.game.choseBall( cb );
      if( resultOfChosingBall.equals( "Ball already chosen" ) ) {
        this.game.removeBall( cb );
        btn.setBackground( DEFAULT );
        // TODO: implement it visually - remove ball chosen
      } else if( resultOfChosingBall.equals( "Set is Full" ) ) {
        // TODO: implement it visually - add ball chosen
      } else {
        // TODO: implement it visually - add ball chosen
        System.out.println( btn.getBackground() );
        btn.setBackground( GREEN );
      }
      String balls = game.getBalls();
      chosenGameLabel.setText( "Chosen Game: " + balls.replace( "[", "").replace( "]", "" ) );
      System.out.println( "Chosen Balls: " + balls );
    }
    // Power Ball Chosen
    else if( toolTip[0].equals( "Power" ) ) {
      Integer pb = Integer.parseInt( toolTip[2] );
      String resultPowerBall = game.chosePowerBall( pb );
      if( resultPowerBall.equals( "PB exists" ) ) {
        btn.setBackground( GREEN );
      } else if( resultPowerBall.equals( "PB chosen" ) ) {
        clearPowerButtons();
        btn.setBackground( GREEN );
      }
      String pbString = game.getPowerBall();
      pbLabel.setText( "Power Ball: " + pbString );
    }
    else { return; }

  }

  private void clearPowerButtons() {
    for ( JButton pBtn : pBtns ) {
      pBtn.setBackground( DEFAULT );
    }
  }

  private void createUIComponents() {
    // TODO: place custom component creation code here



  }

  public static void main( String[] args ) {
    System.out.println( "Whatever line" );
    JFrame frame = new JFrame( "App" );
    frame.setContentPane( new Client().panelMain );
    frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
    frame.pack();
    frame.setVisible( true );
  }
}
