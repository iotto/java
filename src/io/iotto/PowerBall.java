package io.iotto;

import java.util.*;

/**
 * Created by bioduds on 06/08/17.
 */
public class PowerBall extends Game {

  private Set<Integer> balls = new HashSet<Integer>();
  private List sBalls = new ArrayList();
  private Integer pb;

  public PowerBall() {
    super( "Power Ball" );
  }

  public String choseBall( Integer ball ) {

    if( balls.contains( ball ) ) {
      System.out.println( "Ball exists" );
      return "Ball already chosen";
    }
    if( balls.size() == 5 ) {
      System.out.println( "Ball Set is Full" );
      return "Set is Full";
    } else {
      System.out.println( "Ae, rolou" );
      balls.add( ball );
      sBalls = new ArrayList( balls );
      Collections.sort( sBalls );
      return "Ball inserted";
    }
  }

  public void removeBall( Integer ball ) {
    System.out.println("Removing choseBall: " + ball );
    balls.remove( ball );
    sBalls = new ArrayList( balls );
    Collections.sort( sBalls );
  }

  public String chosePowerBall( Integer ball ) {
    if( ball == pb ) {
      return "PB exists";
    } else {
      pb = ball;
      return "PB chosen";
    }
  }

  public String getBalls() {
    return this.sBalls.toString();
  }

  public String getPowerBall() {
    return this.pb.toString();
  }

}