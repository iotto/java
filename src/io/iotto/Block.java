package io.iotto;

import java.sql.Timestamp;

/**
 * Created by bioduds on 06/08/17.
 */
public class Block {

  private Timestamp timestamp;
  private Header header;

  /**
   * Constructor
   */
  public Block( Timestamp timestamp ) {
    this.timestamp = timestamp;
  }

  public void checkBlock() {
    System.out.println( "My timestamp: " + this.timestamp.getTime() );
  }

}

class Header {
  // header
  public int whatever;
}